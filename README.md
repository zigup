# zigup

A posix shell utility to install and update the [Zig](https://ziglang.org/)
compiler.

## Installation

```console
$ wget 'https://git.solidoak.dev/zigup.git/plain/zigup'
$ less zigup # Audit the script.
$ chmod +x zigup
$ mv zigup "$HOME/.local/bin/zigup"
```

Additionally, ensure that all required utility executables are installed. A
non-exhaustive list:

- `jq`
- `minisign`
- `wget`

All other utility executables are typically already installed on most Linux
distributions.

## Usage

Environment variables are used as the input arguments:

- `ZIG_VERSION`: The version which should be installed (default: `master`).
- `PREFIX`: The directory into which the toolchain should be installed (default:
  `$HOME/.local`).

Example:

```console
$ ZIG_VERSION=0.13.0 zigup
```

The toolchain is downloaded to a temporary directory, verified, and extracted to
your data directory (`$XDG_DATA_HOME`). The `zig` executable itself is symlinked
to `$PREFIX/bin/zig`. Ensure that is in your `$PATH`.
