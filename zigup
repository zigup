#!/bin/sh

set -e

cleanup() {
  set +e
  if [ -n "${tmpdir+x}" ]; then
    rm -rf "$tmpdir"
  fi
}

trap 'trap - INT TERM && cleanup' INT TERM EXIT

main() {
  require env
  require head
  require jq
  require ln
  require minisign
  require mkdir
  require mktemp
  require sha256sum
  require tar
  require uname
  require wget

  version="${ZIG_VERSION-master}"

  # Create a temporary workspace.
  tmpdir="$(mktemp -d)"

  # Download the index.json.
  latest_tarball_url="$(
    wget -qO- "${ZIG_INDEX_URL-https://ziglang.org/download/index.json}" \
    | jq -re --arg arch "$(uname -m)-linux" --arg version "$version" '.[$version][$arch].tarball'
  )"
  # Download the tarball.
  env -C "$tmpdir" wget -q "$latest_tarball_url"
  latest_tarball="$tmpdir/${latest_tarball_url##*/}"
  # Download the signature.
  env -C "$tmpdir" wget -q "$latest_tarball_url.minisig"
  # Verify the signature.
  minisign -Vm "$latest_tarball" -P "${ZIG_SIGNING_KEY-RWSGOq2NVecA2UPNdBUZykf1CCb147pkmdtYxgb3Ti+JO/wCYvhbAb/U}"
  # Create a suitable location to extract the tarball.
  hash="$(sha256sum "$latest_tarball" | head -c 10)"
  install_dir="${XDG_DATA_HOME-$HOME/.local/share}/zig/$version-$hash"
  mkdir -p "$install_dir"
  # Extract the tarball, stripping the leading directory.
  tar xvf "$latest_tarball" -C "$install_dir" --strip-components=1
  # Link the zig executable into a well-known location for the PATH.
  ln -sf "$install_dir/zig" "${PREFIX-$HOME/.local}/bin/zig"
}

require() {
  if command -v "$1" >/dev/null 2>&1; then
    return 0
  fi
  printf 'Missing required utility: %s\n' "$1"
  return 1
}

main